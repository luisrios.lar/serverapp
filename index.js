var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override");
    mongoose = require("mongoose");

var cors = require('cors')
var https = require('https');
var http = require('http');
var axios = require('axios');
var LocalStorage = require('node-localstorage').LocalStorage,
localStorage = new LocalStorage('./scratch');
var jwt = require('jsonwebtoken');
var ObjectId = require('mongodb').ObjectID;

var mongoose = require('mongoose');
var connectionstring = 'mongodb+srv://alejo_rios:1bbc1ffdcb5ed@cluster0-s2ngs.mongodb.net/zapatosdb';

var router = express.Router();

var mqtt = require('mqtt');

var client = mqtt.connect("mqtt://ioticos.org", {
    clientId : "mqttjs01Alejo",
    username : 'tgKXq610JQkhnbH',
    password : 'FTbStFSrmqYsTAC',
    clean : true});

const llave = 'millavesecreta'
const jwtExpirySeconds = 300
app.use(cors())
app.use(bodyParser.urlencoded({ extended : false}));
app.use(bodyParser.json());
app.use(methodOverride());

router.use(function (req, res, next) {
    const token = req.headers['access-token'];
    console.log(token)
    if(token){
        jwt.verify(token, llave, (err, decoded) =>{
            if(err){
                return res.json({ mensaje : 'Sesión expirada'});
            }else{
                req.decoded = decoded;
                next();
            }


        });
    } else {
        res.send({
            mensaje: 'Token no valido'
        });
    }
});

app.get('/', function(req, res){
    res.json({ message: 'entrada'});
});
/*-------------------------Board--------------------------------*/
var fabrica_topic =  'e6MTnNydGbx8t9x/1';
var contador_topic = 'e6MTnNydGbx8t9x/2';

client.on('connect', function(connect){
    console.log('Connected to Mqtt');
    //console.log(connack);

    client.subscribe(fabrica_topic, function(err){
        if(!err){
            console.log('subscribe');
            
        }
    });
});

client.on('error', function(err){
    console.log('Error');
});

client.on('message', function(topic, message){
    console.log('topic');
    console.log(topic);
    console.log('message: ');
    console.log(message);
    console.log(message.toString());
});

function intervalFunction(){
    if(client.connected == true){
        var op = {
            retain: false,
            qos : 0           
        };
        var payload = {
            nombre: "Alejandro",
            apellido: "Rios",
            bateria: "100%"
        }
        var enviar = JSON.stringify(payload)
        client.publish(contador_topic, enviar, op);
        
    };
};

setInterval(intervalFunction, 1500);
/*-------------------------Autenticar usuario--------------------------- */

app.post('/autenticar', (req, res) => {
    var datos = req.body
    datos.clave = require('crypto').createHash('md5').update(datos.clave).digest("hex");
    var query = {
        $and:[
            { correo : datos.correo},
            { clave : datos.clave}
    ]
    }
    Reg.findOne(query, function(err, result){
        if(err){
            console.log("Error en la consulta")
            res.send("Error")
        }else{
            console.log("Consulta OK")
            if(result){
                const payload = {
                    correo: result.correo
                };
                const token = jwt.sign(payload, llave, {
                    algorithm : 'HS256',
                    expiresIn: jwtExpirySeconds
                });
                res.json({
                    mensaje: 'Autenticación correcta',
                    token: token,
                    nombre: result.nombre
                });
            }else{
                res.json({ mensaje: "Usuario o contraseña incorrectos"})
            }
        }
    })
});
localStorage.setItem("algo", "mas")
/*------------------------------Tarjeta-------------------------------- */
app.post('/tarjeta', (req, res) => {

    res.send(1)
});

/*-------------------------Consultar usuario--------------------------- */

app.post('/consulta', (req, res) => {

    var datos = req.body
    var query = { correo : datos.correo }
    Reg.find(query, function(err, result){
        if(err){
            console.log("Error en la consulta")
            res.send("Error")
        }else{
            if(result == ""){
                res.send("No se ha registrado")
            }else{
                console.log("Bienvenido")
                res.send(result)
            }
        }
    })
});
/*-------------------------Todos los usuarios--------------------------- */
app.post('/todos', router, (req, res) => {

    var datos = req.body
    var query = {
        $or:[
            { rol : 1},
            { rol : 2},
            { rol : 3}
    ]
    }
    Reg.find(query, function(err, result){
        if(err){
            console.log("Error en la consulta")
            res.send("Error")
        }else{
            if(result == ""){
                res.send("No se ha registrado")
            }else{
                console.log("Bienvenido")
                res.send(result)
            }
        }
    })

});
/*-------------------------Agregar codigo--------------------------- */

app.post('/agregar', (req, res) => {
    var datos = req.body
    var myData = new Cod(datos)
    myData.activo = 0
    myData.save().then(item => {
        console.log("Código agregado correctamente")
        var payload = {
            "mensaje" : "Agregado correctamente"
        }
        res.send(payload)
    })
    .catch(err => {
        m = "ERROR"
        console.log("No pudo ser guardado")
        var payload = {
            "mensaje" : "Error agregando el código"
        }
        res.send(payload)
    })
})
/*-------------------------Activar codigo--------------------------- */
app.post('/activar',  (req, res) => {
    var datos = req.body
    var query = { codigo : datos.codigo }
    Cod.find(query, function(err, result) {
        if(err){
            console.log("Error en la consulta")
            res.send("Error")
        }else{
            if(result == ""){
                res.send("No se encontro el código")
            }else{
                console.log("Codigo activado")
                Cod.collection.update(query, 
                    {
                        "codigo" : datos.codigo,
                        "activo" : 1

                })
                res.send("Codigo activado correctamente")           
            }
        }
    })
});
/*-------------------------Modificar Usuario---------------------------*/
app.post('/modificar', (req, res) => {
    var datos = req.body
    var query = { _id : ObjectId(datos.id) }
    Reg.find(query, function(err, result) {
        if(err){
            console.log("Error en la consulta")
            res.send("Error")
        }else{
            if(result == ""){
                res.send("No se encontro el usuario")
            }else{
                console.log("Codigo activado")
                datos.clave = require('crypto').createHash('md5').update(datos.clave).digest("hex")
                Reg.collection.updateOne(query, 
                        [
                            {
                            $set: {"nombre": datos.nombre,
                            "apellido": datos.apellido,
                            "rol": datos.rol,
                            "correo": datos.correo,
                            "clave": datos.clave,
                            "ciudad": datos.ciudad,
                            "codigo": datos.codigo
                            }
                            }
                        ])
                res.send("Usuario modificado correctamente")           
            }
        }
    })
});

/*--------------------------Eliminar usuario--------------------------- */
app.post('/eliminar', (req, res) => {

    var datos = req.body
    var query = { correo : datos.correo }
    Reg.find(query, function(err, result){
        if(err){
            console.log("Error en la consulta")
            res.send("Error")
        }else{
            if(result == ""){
                res.send("No se ha encontrado")
            }else{
                console.log("Removido")
                Reg.collection.deleteOne(query,{
                    "correo": datos.correo
                })
                res.send("Removido")
            }
        }
    })
});

/*-------------------------Registrar usuario--------------------------- */
app.post('/registro', (req, res) => {
    var activado = 0
    var datos = req.body
    var payload
    datos.fecha = getDataTime()
    var myData = new Reg(datos)
    myData.clave = require('crypto').createHash('md5').update(myData.clave).digest("hex");
    var query = { correo : myData.correo }
    var query2 = { codigo : myData.codigo }

    Cod.find(query2, function(err, result){
        if(err){
            console.log("Error")
        }else{
            if(result == ""){
                console.log("Codigo no encontrado");
                payload = { "mensaje" : "Codigo no encontrado" }
                res.send(payload)
            }else{
                if(result.toString().search("codigo: '"+myData.codigo+"', activo: 0") != -1){
                    activado = 1;
                    console.log("correcto");
                    
                }else{
                    console.log("No está activo este codigo");                    
                }
            }
        }
    })
    Reg.find(query, function(err, result){
        if(err){
            console.log("Error")
        }else{
            if(result == "" && activado == 1){
                myData.save().then(item => {
                    console.log("Registro exitoso")
                    payload = { "mensaje" : "Registro exitoso" }
                    res.send(payload)
                })
                .catch(err => {
                    console.log("Surgió un problema")

                })
            }else{
                if (activado != 0){
                console.log("Correo ya registrado")
                payload = { "mensaje" : "Correo ya registrado" }
                res.send(payload)
                }
            }
        }
    })
    
});

/*-------------------------Fin registrar--------------------------- */
const PORT = process.env.PORT || 3000;
var httpServer = http.createServer(app).listen(PORT, function () { //process.env.PORT || 3000
    console.log(getDataTime());
    console.log('Our app is running on port '+ PORT);
});

var connectedtodb = false;

mongoose.connect(connectionstring,{ useNewUrlParser: true}, function(err, res) {
    if(err) {
        console.log('Error: connecting to database. ' + err);
    }else{
        console.log('Connected to Atlas')
        connectedtodb = true;
    }
});

var nameSchema = new mongoose.Schema({
    nombre: String,
    apellido: String,
    rol: Number,
    correo: String,
    clave: String,
    fecha: String,
    ciudad: String,
    codigo: String
});

var codeSchema = new mongoose.Schema({
    codigo: String,
    activo: Number
})

var Reg = mongoose.model("Usuarios", nameSchema);
var Cod = mongoose.model("Codigos", codeSchema);

function getDataTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}